﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    public GameObject spawnItem;
    public Vector3 spawnRange;
    public float spawnWait;
    public float spawnMostWait;
    public float spawnLeastWait;
    public int startWait;
    public bool stop;

    int randEnemy;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(WaitSpawner());
    }

    // Update is called once per frame
    void Update()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
    }

    IEnumerator WaitSpawner()
    {
        yield return new WaitForSeconds(startWait);

        while (!stop)
        {
            Vector3 spawnPosition = RandomCircle(new Vector3(0,0,0), 7.0f);

            Debug.Log(spawnPosition);

            var obj = Instantiate(spawnItem, spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);
            obj.GetComponent<Renderer>().material.color = Color.green;

            yield return new WaitForSeconds(spawnWait);
        }
    }

    Vector3 RandomCircle(Vector3 center, float radius)
    {
        Vector3 direction = Random.onUnitSphere;
        float distance = 0;
        while (Vector3.Distance(direction, center) < 3)
        {
            direction = Random.onUnitSphere;
            direction.y = Mathf.Clamp(direction.y, 0.2f, 1.5f);
            direction.x = Mathf.Clamp(direction.x, -radius, radius);
            direction.z = Mathf.Clamp(direction.z, -radius, radius);
            distance = 2 * Random.value + 5.5f;
            direction = direction * distance;
        }
        return direction;
    }
}
